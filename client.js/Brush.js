function Distance(X1,Y1,X2,Y2){
    return Math.sqrt(Math.pow(X1-X2,2)+Math.pow(Y1-Y2,2));
  }
   
function Brush(){
  this.BoundingBoxSize=0;
  var BrushSize=0;
  var Settings={
    Size:1,
    Softness:1,
    Elliptical:0,
    Angle:0,
    Color:(new Color)
  };
  Settings.Color.Hex='000000';
  
  var Canvas=document.createElement('canvas');
  
  var context;
  
  ['Color','Size','Softness','Elliptical','Angle'].forEach(function(element){
    Object.defineProperty(this,element,{
      enumerable:true,
      configurable:false,
      get:function(){
        return Settings[element];
      },
      set:function(value){
        Settings[element]=value;
        Check();
        Update();
      }
    });
  },this);
  
  Object.defineProperty(this,'BoundingBoxSize',{
      enumerable:true,
      configurable:false,
      get:function(){
        return BrushSize;
      },
      set:function(value){}
  });
  
  
  function Check(){
    if(isNaN(Settings.Size)||Settings.Size<1||Settings.Size>50)
      throw "Brush.Check: Size is not defined or out of range";
    if(isNaN(Settings.Softness)||Settings.Softness<1||Settings.Softness>100)
      throw "Brush.Check: Softness is not defined or out of range";
    if(isNaN(Settings.Elliptical)||Settings.Elliptical<0||Settings.Elliptical>100)
      throw "Brush.Check: Softness is not defined or out of range";
    if(isNaN(Settings.Angle)||Settings.Angle<0||Settings.Angle>180)
      throw "Brush.Check: Angle is not defined or out of range";
    if(!(Settings.Color instanceof Color))
      throw "Brush.Check: Color is not a valid";
  }
  
  function Dimensions(){
    var canvasSize=0;
    var fade=Math.pow((1-Settings.Softness/100)*2,2);
    for(var distance=0;distance<100;distance++){
      var t=-fade*(distance-Settings.Size);
      t=1+Math.pow(Math.E,t);
      canvasSize=distance;
      if(!Math.floor(255-255/t))
        break;
    }
    return canvasSize*2;
  }
  

  function Update(color,tmpOptions){
    BrushSize=Dimensions();
    Canvas.height=BrushSize;
    Canvas.width=BrushSize;
    context=Canvas.getContext('2d');
    
    var tmpCanvas=document.createElement('canvas');
    tmpCanvas.id='foobar';
    tmpCanvas.height=Canvas.height;
    tmpCanvas.width=Canvas.width;
    var tempContext=tmpCanvas.getContext('2d');
    
    tempContext.fillStyle='#'+Settings.Color.Hex;
    tempContext.fillRect(0,0,tmpCanvas.width,tmpCanvas.height);
    
    var fade=Math.pow((1-Settings.Softness/100)*2,2);
    var elliptical=Settings.Elliptical/11.1111+1;
    
    var haltbrush=BrushSize/2;
    var data=tempContext.getImageData(0,0,tmpCanvas.width,tmpCanvas.height); 
    for(var y=0;y<BrushSize;y++){
      for(var x=0;x<BrushSize;x++){
        var distance=Distance(0,0,(x-haltbrush)*elliptical,y-haltbrush)-Settings.Size;
        var t=(1+Math.pow(Math.E,-fade*distance));
  //      var alpha=Math.floor(255-255/t)/255;
        var pixeloffset=4*x+(4*BrushSize*y);
        data.data[pixeloffset+3]=Math.floor(255-255/t);
  //      data.data[pixeloffset+2]*=alpha;
  //      data.data[pixeloffset+1]*=alpha;
  //      data.data[pixeloffset+0]*=alpha;
      }
    }

    tempContext.putImageData(data,0,0);
    
    context.clearRect(0,0,Canvas.width,Canvas.height);
 //   context.save();
 //   context.translate(haltbrush,haltbrush);
 //   context.rotate(tmpOptions.angle*Math.PI/180);
 //   context.translate(-haltbrush,-haltbrush);
    context.drawImage(tmpCanvas,0,0);
 //   context.restore();
  }
  
  this.Draw=function(To,X,Y){
    To.drawImage(Canvas,X-BrushSize/2,Y-BrushSize/2);
  };
  
 // this.Initialize(options);
  Update();
}