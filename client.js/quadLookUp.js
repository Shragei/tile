function QuadLookUp(path){
  var walk=new Array;
  var pathString=undefined;
  var Yaxis=1;
  var Xaxis=0;
  
  function MapQuadrantXY(quadrant){
    switch(quadrant){
      case 1:
        return [1,1];
      case 2:
        return [1,-1];
      case 3:
        return [-1,-1];
      case 4:
        return [-1,1];
      default:
        throw "QuadLookUp.MapQuadrantXY: A Qudrant is out of bounds";
    }
  }
  
  function MapXYQuadrant(XY){
    if(XY[0]>0){
      if(XY[1]>0)
        return 1;
      else
        return 2;
    }else{
      if(XY[1]<0)
        return 3;
      else
        return 4;
    }
  }
  
  function Walk(XY,on,path){
      var level=-1;
     // for(var i=0;i<path.length;i++)
     //  path[i]=MapQuadrantXY(path[i]);
      /* walk back up the tree looking for the new direction to no go out of bounds
       * once found flip the XY for the quad and replace it
       */
      for(level=path.length-1;level>-1;level--){
        var currentXY=path[level];
        if(currentXY[on]+XY[on]==0){
          currentXY[on]*=-1;
          path[level]=currentXY;
          break;
        }
      }
      
      if(level==-1)
        throw "QuadLookUp.Walk: exhausted path depth";
        
      XY[on]*=-1;
      
      // walk back down the tree setting the quadrant
      
      for(level++;level<path.length;level++){
        var currentQuadrant=path[level];
        currentQuadrant[on]=XY[on];
        path[level]=currentQuadrant;
      }
      
  //    for(var i=0;i<path.length;i++)
  //      path[i]=MapXYQuadrant(path[i]);
      return path;      
    }
  
  function CloneWalk(){
    var clone=walk.slice(0);
    for(var i=0;i<clone.length;i++)
      clone[i]=clone[i].slice(0);
    return clone;
  }
  
  this.SetPath=function(path){
    if(path instanceof Array){
      walk=path;
    }else if(typeof path === 'string'){
      walk=path.split(',');
      for(var i=0;i<walk.length;i++)
        walk[i]=MapQuadrantXY(parseInt(walk[i]));
    }else{
      throw "QuadLookUp.SetPath: path is not an array of string";
    }
    pathString=undefined;
  }
  
  this.Stringify=function(){
    if(typeof pathString!=='string'){
      var path=walk.slice(0);
      for(var i=0;i<path.length;i++)
        path[i]=MapXYQuadrant(path[i]); 
      pathString=path.join(',');
    }
    return pathString;
  }
  
  this.GetRaw=function(){
    return walk.slice(0);
    
  }
  
  this.Neighbour=function(direction){
    var neighbourPath=CloneWalk();
    switch(direction.toLowerCase()){
      default: 
        throw "QuadLookUP.Neighbour: Argument is not a valid compass direction";
      case 'north':
        neighbourPath=Walk([0,1],Yaxis,neighbourPath);
        break;
      case 'south':
        neighbourPath=Walk([0,-1],Yaxis,neighbourPath);
        break;
      case 'east':
        neighbourPath=Walk([1,0],Xaxis,neighbourPath);
        break;
      case 'west':
        neighbourPath=Walk([-1,0],Xaxis,neighbourPath);
        break;
        
    }
    return new QuadLookUp(neighbourPath);
  }
  
  if(typeof path!=='undefined')
    this.SetPath(path);
}