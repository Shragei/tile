/*
 * tileloader.fetch(path) returns the dom element for the path
 * tileLoader.tileSize the size of the tile
 */

function Field(viewport,tileLoader,initPath){
  var tileHash=new Array;
  var tileSize=0;  
  var halfTileSize=0;
  var maxDistance=0;
  
  var viewportCenterY;
  var viewportCenterX;

  function Distance(X1,Y1,X2,Y2){
    return Math.sqrt(Math.pow(X1-X2,2)+Math.pow(Y1-Y2,2));
  }
  
  function OutOfBounds(X,Y){
    return Math.sqrt(Math.pow(X,2)+Math.pow(Y,2))>maxDistance;
  }

  function InvertDirection(direction){
    switch(direction){
      case 'North':return 'South';
      case 'East':return 'West';
      case 'South':return 'North';
      case 'West':return 'East';
    }
  }

  function NewTile(path){
    var tile=tileLoader.fetch(path);
     
    
    tile.neighbours={'North':false,'East':false,'South':false,'West':false};
    
    tile.path=path;

    tile.load=true;
    
    tile.X=0;
    tile.Y=0;
    

    tile.DOM.style.position='absolute';
    tile.DOM.style.height=tileSize+'px';
    tile.DOM.style.width=tileSize+'px';
    
    return tile;
  }
  
  function RemoveTile(tile){
  	tileLoader.remove(tile.path);
  	viewport.removeChild(tile.DOM);
  	
    delete tileHash[tile.path.Stringify()];
    for(var direction in tile.neighbours){
      if(!tile.neighbours.hasOwnProperty(direction))
        continue;
      var neighbourPath=tile.path.Neighbour(direction);
      var neighbourStringify=neighbourPath.Stringify();
      if(neighbourStringify in tileHash)
        tileHash[neighbourStringify].neighbours[InvertDirection(direction)]=false;
    }
  }
  
  function TestNeighbours(tile){
    for(var direction in tile.neighbours){
      if(!tile.neighbours.hasOwnProperty(direction))
        continue;
      if(tile.neighbours[direction]===false){
        var neighbourPath=tile.path.Neighbour(direction);
        var neighbourStringify=neighbourPath.Stringify();
        if(neighbourStringify in tileHash){
          tileHash[neighbourStringify].neighbours[InvertDirection(direction)]=true;
          tile.neighbours[direction]=true;
        }
      }
    }
  }
  
  
  function PlaceTile(tile){
    
    var X=viewportCenterX;
    var Y=viewportCenterY;
    
    X-=halfTileSize;
    Y-=halfTileSize;
    
    X+=tile.X;
    Y-=tile.Y;
    
    tile.DOM.style.left=X+'px'; 
    tile.DOM.style.top=Y+'px';
  }
  
  function SpawnNeighbours(tile){
    var neighbourTiles=new Array;
    
    for(var direction in tile.neighbours){
      if(!tile.neighbours.hasOwnProperty(direction))
        continue;
      if(tile.neighbours[direction]===false){
        var neighbourPath=tile.path.Neighbour(direction);
        
        var X;
        var Y;
               
        if(neighbourPath.Stringify() in tileHash){
          console.warn('Field.SpawnNeighbours: missed a tile hinting for direction, '+direction);
          tile.neighbours[direction]=true;
          continue;
        }
        
        switch(direction){
          case 'North':
            X=tile.X;
            Y=tile.Y+tileSize;
            break;
          case 'East':
            X=tile.X+tileSize;
            Y=tile.Y;
            break;
          case 'South':
            X=tile.X;
            Y=tile.Y-tileSize;
            break;
          case 'West':
            X=tile.X-tileSize;
            Y=tile.Y;
            break;
        }
        
        if(!OutOfBounds(X,Y)){
          var neighbourTile=NewTile(neighbourPath);
          tileHash[neighbourPath.Stringify()]=neighbourTile;
          
          neighbourTile.neighbours[InvertDirection(direction)]=true;
          tile.neighbours[direction]=true;
          
          neighbourTile.X=X;
          neighbourTile.Y=Y;
          
          TestNeighbours(neighbourTile);
          
          neighbourTiles.push(neighbourTile);
        }
      }
    }
    return neighbourTiles;
  }
  
  var lastBrushTileFIFO=[];
  this.BrushLocation=function(brush,X,Y){
    X=X-viewportCenterX;
    Y=viewportCenterY-Y;
    var halfBrushSize=brush.BoundingBoxSize/2;
    var halfTileSize=tileLoader.tileSize;
    
    while(lastBrushTileFIFO.length){
      var lastBrushTile=lastBrushTileFIFO.shift();

      if(lastBrushTile in tileHash)
        tileHash[lastBrushTile].BrushClear();
    }
    
    for(var path in tileHash){
      var tile=tileHash[path];
      var tileX=X-tile.X;
      var tileY=Y-tile.Y;
      if(tileX-halfBrushSize<halfTileSize&&
         tileX+halfBrushSize>-halfTileSize&&
         tileY-halfBrushSize<halfTileSize&&
         tileY+halfBrushSize>-halfTileSize){
   
        tile.BrushLocation(brush,X-tile.X,Y-tile.Y);     
        
        lastBrushTileFIFO.push(path);
      }
    }
  };
  
  this.BrushClear=function(){
    for(var path in tileHash){
      var tile=tileHash[path];
      tile.BrushClear();
    }
  };
  
  function clip(points,bounds){//Cohen-Sutherland clipping algorithm
    var xmax=ymax=bounds;
    var xmin=ymin=bounds*-1;
    
    function clipOpCode(X,Y,bounds){
      var code=0; //Inside
      if(X<-bounds)
        code|=1; //left
      else if(X>bounds)
        code|=2; //right
      if(Y<-bounds)
        code|=4;  //bottom
      if(Y>bounds)
        code|=8;  //top
      return code;
    }
    
    var opCode1=clipOpCode(points.X1,points.Y1,bounds);
    var opCode2=clipOpCode(points.X2,points.Y2,bounds);
    
    var accept=false;
    
    do{
      if(!(opCode1|opCode2))
        return points;
      else if(opCode1&opCode2)
        return false;
      else{
        var x,y;
        
        var opCode=opCode1?opCode1:opCode2;
        if(opCode&8){
          x = points.X1 + (points.X2 - points.X1) * (bounds - points.Y1) / (points.Y2 - points.Y1);
          y = bounds;
        }else if(opCode&4){
          x = points.X1 + (points.X2 - points.X1) * (-bounds - points.Y1) / (points.Y2 - points.Y1);
          y = -bounds;
        }else if(opCode&2){
          y = points.Y1 + (points.Y2 - points.Y1) * (bounds - points.X1) / (points.X2 - points.X1);
          x = bounds;
        }else if(opCode&1){
          y = points.Y1 + (points.Y2 - points.Y1) * (-bounds - points.X1) / (points.X2 - points.X1);
          x = -bounds;
        }
        
        if(opCode===opCode1){
          points.X1=x;
          points.Y1=y;
          opCode1=clipOpCode(points.X1,points.Y1,bounds);
        }else{
          points.X2=x;
          points.Y2=y;
          opCode2=clipOpCode(points.X2,points.Y2,bounds);
        }
      }
    }while(true);
  }
  
  this.BrushApply=function(brush,X1,Y1,X2,Y2){
    var halfTileSize=128;
    var halfBrushSize=brush.BoundingBoxSize/2;
    var boundingEdge=halfTileSize+brush.BoundingBoxSize;
  	X1=X1-viewportCenterX;
    Y1=viewportCenterY-Y1;
    X2=X2-viewportCenterX;
    Y2=viewportCenterY-Y2;
    
    
  	for(var path in tileHash){
  		var tile=tileHash[path];
      
      var points={'X1':X1-tile.X,'Y1':Y1-tile.Y,'X2':X2-tile.X,'Y2':Y2-tile.Y};
      points=clip(points,boundingEdge);
      if(points===false)
        continue;
  		tile.BrushApply(brush,points.X1,points.Y1,points.X2,points.Y2);
  	}
  };
  
  this.ViewportUpdate=function(){
    var rect=viewport.getBoundingClientRect();
    tileSize=tileLoader.tileSize;
    halfTileSize=tileSize/2;
    maxDistance=Distance(0,0,rect.width/2,rect.height/2);
    //maxDistance+=Distance(0,0,tileSize,tileSize)*2;
    
    
    viewportCenterX=rect.left+rect.width/2;
    viewportCenterY=rect.top+rect.height/2;
  };
  
  this.Move=function(X,Y){
  //  var newTileBin=new Array;
    var newTileBin=[];
    for(var key in tileHash){
      if(!tileHash.hasOwnProperty(key))
        continue;
      var tile=tileHash[key];
      if(tile.load)
        continue;
      tile.X+=X;
      tile.Y+=Y;
      if(OutOfBounds(tile.X,tile.Y)){
        RemoveTile(tile);
        continue;
      }
      
      newTileBin.push.apply(newTileBin,SpawnNeighbours(tile));
    }
    while(newTileBin.length){
      var newTile=newTileBin.pop();
      newTileBin.push.apply(newTileBin,SpawnNeighbours(newTile));
    //  TestNeighbours(newTile);
      viewport.appendChild(newTile.DOM);
    }
    
    for(var key in tileHash){
      if(!tileHash.hasOwnProperty(key))
        continue;
      PlaceTile(tileHash[key]);
      tileHash[key].load=false;
    }
  };
  
  this.ViewportUpdate();

  
  var newTileBin=new Array;
  newTileBin.push(NewTile(initPath));
  
  do{
    var tile=newTileBin.pop();
    
    tile.load=false;
    
    newTileBin.push.apply(newTileBin,SpawnNeighbours(tile));
    TestNeighbours(tile);
    
    PlaceTile(tile);
    viewport.appendChild(tile.DOM);
    
    tileHash[tile.path.Stringify()]=tile;
  }while(newTileBin.length);
}