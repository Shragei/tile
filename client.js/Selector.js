function Selector(name,actions){
  this.parent=undefined;
  this.name=name;

  var widget=$();
  var centerX=0;
  var centerY=0;
  var rect;
  var angle=0;
  var step=360/actions.length;
  
  function hideHighlights(){
    for(var i=0;i<actions.length;i++){
      widget.find('#'+widget.attr('id')+'-'+actions[i].name+' .hightlight').hide();
    }    
  }

    
  var selector=function(event){
    var X=event.clientX-(rect.left+rect.width/2);
    var Y=event.clientY-(rect.top+rect.height/2);
    var distance=Math.sqrt(Math.pow(X,2)+Math.pow(Y,2));
    
    angle=(Math.atan2(X,Y)*(180/Math.PI)*-1)+180;
    for(var offset=0;offset<=360;offset+=step){
      if(angle<(step/2)+offset&&angle>-(step/2)+offset){
        angle=offset;
        break;
      }
    }
    var action=(angle/step)%actions.length;
    
    if(distance<60){
      hideHighlights();
    }else if(distance>106){
      widget.unbind('mousemove');
      this.Hide();
      var action=(angle/step)%actions.length;
      actions[action].widget.Show(event.clientX,event.clientY);
    }else{
      hideHighlights();
      widget.find('#'+widget.attr('id')+'-'+actions[action].name+' .hightlight').show();
    }
  }.bind(this);
  

  this.Hide=function(){
    widget.hide();
    widget.remove();

  }		
  
  this.Show=function(X,Y){
    widget.show();
    $('body').append(widget);	
   
    widget.css('left',X-115+'px');
    widget.css('top',Y-115+'px');
    
    widget.mousemove(selector);
    widget.mouseup(this.Hide);
    
    rect=widget[0].getBoundingClientRect();
  }

 $.get('/resources/widget.svg',function(widgets){
    widget=$(widgets).find('#Selector');
    widget.hide();
    widget.attr('id',widget.attr('id')+'-'+name);
    
    var templateitem=widget.find('.item');
    templateitem.remove();
    for(var i=0;i<actions.length;i++){
      var item=templateitem.clone();
      item.attr('id',widget.attr('id')+'-'+actions[i].name);
      item.find('text').text(actions[i].name);
      item.attr('transform','rotate('+step*i+',115,115)');
      item.find('.counter').attr('transform','rotate('+(step*i*-1)+')');
      item.appendTo(widget);
    }
  });
}