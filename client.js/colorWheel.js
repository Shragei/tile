function Distance(X1,Y1,X2,Y2){
    return Math.sqrt(Math.pow(X1-X2,2)+Math.pow(Y1-Y2,2));
  }
  
function ColorWheel(color){
	var widget;
  var rect; 
  var wheelID='';
  var centerX=0;
  var centerY=0;
  

  function UpdateGradient(){
  	var wheels=['Saturation','Value'];
  	while(wheels.length){
	  	var gradeTop=new Color();
	    var gradeBottom=new Color();
	    gradeTop.Hue=gradeBottom.Hue=color.Hue;

	    var wheelID=wheels.pop();
	    switch(wheelID){
	    	case 'Saturation':
		      gradeTop.Value=1;
		      gradeBottom.Value=1;
		      gradeTop.Saturation=1;
		      gradeBottom.Saturation=0;
		      break;
	      case 'Value':
	        gradeTop.Value=1;
	        gradeBottom.Value=0;
	      //  gradeMiddle.Value=0.5
	        gradeTop.Saturation=gradeBottom.Saturation=color.Saturation;
	    }
	    
	 // 	gradeTop.HSVtoRGB();
	 // 	gradeTop.RGBtoHex();
	 // 	gradeBottom.HSVtoRGB();
	 // 	gradeBottom.RGBtoHex();
	  	
	  	var gradient=widget.find('#ColorPicker'+wheelID+'Gradient');
	  	gradient.find("[offset='0']").attr('stop-color','#'+gradeTop.Hex);
	  	gradient.find("[offset='1']").attr('stop-color','#'+gradeBottom.Hex);
    }
  }
  function UpdateOculi(OculiID,H,S,V){
  	var Oculi=new Color();
  	Oculi.Hue=H;
  	Oculi.Saturation=S;
  	Oculi.Value=V;

  	widget.find('#ColorPicker'+OculiID+'Oculi').attr('fill','#'+Oculi.Hex); 
  }
  
  function Update(){
  	UpdateOculi('Hue',color.Hue,1,1);
  	UpdateOculi('Saturation',color.Hue,color.Saturation,1);
  	UpdateOculi('Value',color.Hue,color.Saturation,color.Value);
  	UpdateOculi('',color.Hue,color.Saturation,color.Value);
  	UpdateGradient();
  }
  
  function SetWheel(wheelID,delta){
  	var oldRotation=widget.find('#ColorPicker'+wheelID).attr('transform').substr(7);
  	oldRotation=Number(oldRotation.substr(0,oldRotation.length-9));
  	var newRotation=(oldRotation+delta)%360;
  	widget.find('#ColorPicker'+wheelID).attr('transform','rotate('+(newRotation)+',115,115)');
  }
  
  var lastAngle=0;

  function UpdateWheel(event){
  	if(wheelID==='') return;
  	if(event.buttons===0){
  		widget.parent().unbind('mousemove',UpdateWheel);
  	}
  	//Get the angular position of the wheel
  	var oldRotation=widget.find('#ColorPicker'+wheelID).attr('transform').substr(7);
  	oldRotation=Number(oldRotation.substr(0,oldRotation.length-9));
  	
  	var angle=Math.atan2(event.clientY-centerY,event.clientX-centerX)*(180/Math.PI)+180;
  	
  	
    var delta=(angle-lastAngle)%360;
    delta=delta>180?360-delta:delta;
    var wheelAngle=(oldRotation+delta)%360;

    lastAngle=angle;
    
  	switch(wheelID){
  		case 'Hue':
  		  color.Hue=(360-wheelAngle)%360;
  		  break;
  		case 'Saturation':	    
  		  color.Saturation=(1/Math.PI)*Math.asin(Math.cos(2*Math.PI*(wheelAngle/360)))+0.5;
  		  break;
  		case 'Value': 
  		  color.Value=(1/Math.PI)*Math.asin(Math.cos(2*Math.PI*(wheelAngle/360)))+0.5;
  		  break;
  	}

    Update();

  	widget.find('#ColorPicker'+wheelID).attr('transform','rotate('+(wheelAngle)+',115,115)');
  	
  };
  function SetEvents(wheelid){
	  widget.find('#ColorPicker'+wheelid).mousedown(function(event){
	  	wheelID=wheelid;
	  	lastAngle=Math.atan2(event.clientY-centerY,event.clientX-centerX)*(180/Math.PI)+180;
	  	widget.parent().mousemove(UpdateWheel);
	  });
  }

  function UnbindUpdateWheel(){
    wheelID='';
		widget.parent().unbind('mousemove',UpdateWheel);
  }

  $.get('/resources/widget.svg',function(widgets){
    widget=$(widgets).find('#ColorPicker');
    
	  SetEvents('Hue');
	  SetEvents('Value');
	  SetEvents('Saturation');
	
	  Update();
	  
	  SetWheel('Hue',color.Hue);
	  SetWheel('Saturation',(1-color.Saturation)*180);
	  SetWheel('Value',(1-color.Value)*180);
	      
    widget.delegate('*','dragstart',function(event){event.preventDefault();});
  });
  
  
  this.Hide=function(){
    widget.hide();
    widget.remove();
    widget.parent().unbind('mouseup',UnbindUpdateWheel);
  }
  
  this.Show=function(X,Y){
    widget.show();
    $('body').append(widget);	
    widget.parent().mouseup(UnbindUpdateWheel);
    
    centerX=X;
    centerY=Y;
    
    widget.css('left',X-115+'px');
    widget.css('top',Y-115+'px');
    
    rect=widget[0].getBoundingClientRect();
    visable=true;
    SetEvents('Hue');
	  SetEvents('Value');
	  SetEvents('Saturation');
	
	  Update();
	  
	
    
  }
}