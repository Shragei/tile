function Tile(path,canvasId){
  var Forground;
  var brushCanvas;
  
  this.Path=path;
  this.DOM=document.createElement('div');
  this.DOM.className='tile';
  this.TileSize=256;
  this.Tainted=false;
  var halfTileSize=this.TileSize/2;
  var flushTimeout=0;
    
  this.BrushLocation=function(brush,X,Y){
       brushContext.clearRect(0,0,this.TileSize,this.TileSize);
       brush.Draw(brushContext,halfTileSize+X,halfTileSize-Y);
  };
  
  this.BrushClear=function(){
    brushContext.clearRect(0,0,this.TileSize,this.TileSize);
  };
  this.BrushApply=function(brush,X1,Y1,X2,Y2){
    if(X1===X2){
      var YStart=Y1<Y2?Y1:Y2;
      var YEnd=Y1>Y2?Y1:Y2;
      
      for(var Y=YStart;Y<=YEnd;Y+=1)
        brush.Draw(forgroundContext,halfTileSize+X1,halfTileSize-Y);
    }else{
      var m=(Y2-Y1)/(X2-X1);
      var XStart=X1<X2?X1:X2;
      var XEnd=X1>X2?X1:X2;
      
      var YOffset=XStart===X1?Y1:Y2;
      YOffset-=XStart*m;
      
      var Stepping=Math.sqrt(Math.pow(X1-X2,2)+Math.pow(Y1-Y2,2));
      
      Stepping=(XEnd-XStart)/(Stepping);
      
      for(var X=XStart;X<=XEnd;X+=Stepping){
        var Y=X*m+YOffset;
        brush.Draw(forgroundContext,halfTileSize+X,halfTileSize-Y);
      }
    }
    if(this.Tainted===false){
      this.Tainted=true;
      flushTimeout=window.setTimeout(function(){
        flushTimeout=0;
        this.Flush();
      }.bind(this),300000);
    }
    this.Tainted=true;
  };
  
  this.Flush=function(){
    if(this.Tainted){
      if(flushTimeout)
         window.clearTimeout(flushTimeout);
      
      console.log('flushing tile to server');
      Forground.toBlob(function(blob){
        var oReq = new XMLHttpRequest();
        oReq.open("PUT","/canvas/"+canvasId+"/"+this.Path.Stringify()+'/root', true);
        
        oReq.send(blob);
      }.bind(this));
      this.Tainted=false;
    }
  };

  var oReq = new XMLHttpRequest();
  oReq.open("GET", "/canvas/"+canvasId+"/"+this.Path.Stringify(), true);
  oReq.responseType = "arraybuffer";
  oReq.setRequestHeader('Authorization','Token lkajhsdfalkjhadfasdfa');
  var HTTPRequest=function(event){
    if (oReq.readyState === 4) {
      if (oReq.status === 200) {
     //   console.profile();
        var data=bson().BSON.deserialize(Uint8Array(oReq.response));
     //   console.profileEnd();

        var url=URL.createObjectURL(new Blob([data[0].data.buffer],{type:'image/png'}));
        var tmpImage=new Image();
        tmpImage.onload=function(){
          forgroundContext.drawImage(tmpImage,0,0);
          URL.revokeObjectURL(url);
        };
        tmpImage.src=url;
      }else if(oReq.status===204){
    //    console.log('tile '+this.Path.Stringify()+' is not stored');
      }
    }
  }.bind(this);

  oReq.addEventListener('load',HTTPRequest,false);
  oReq.send(null);
  
  Forground=document.createElement('canvas');
  Forground.height=this.TileSize;
  Forground.width=this.TileSize;
  Forground.style.height=Forground.style.width=this.TileSize+'px';
  var forgroundContext=Forground.getContext('2d');
  this.DOM.appendChild(Forground);
  
  brushCanvas=document.createElement('canvas');
  brushCanvas.height=this.TileSize;
  brushCanvas.width=this.TileSize;
  brushCanvas.style.height=brushCanvas.style.width=this.TileSize+'px';
  brushCanvas.style.position='relative';
  brushCanvas.style.top=-this.TileSize+'px';
  var brushContext=brushCanvas.getContext('2d');
  this.DOM.appendChild(brushCanvas);
}