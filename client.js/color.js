function Color(){
  var color={
    Red:0,
    Green:0,
    Blue:0,
    Hue:0,
    Saturation:0,
    Lightness:0,
    Value:0,
    Hex:''
  };
  this.Red=0;
  this.Green=0;
  this.Blue=0;
  this.Hue=0;
  this.Saturation=0;
  this.Lightness=0;
  this.Value=0;
  this.Hex='';
  
  function hue2rgb(p, q, t){
    if(t < 0) t+=1;
    if(t > 1) t-=1;
    if(t < 1/6) return p+(q-p)*6*t;
    if(t < 1/2) return q;
    if(t < 2/3) return p+(q-p)*(2/3-t)*6;
    return p;
  }

  function SwitchCases(h,c,x){
  	switch(Math.floor(h)){
    	case 0:
    	  return {r:c,g:x,b:0};
    	case 1:
    	  return {r:x,g:c,b:0};
    	case 2:
    	  return {r:0,g:c,b:x};
    	case 3:
    	  return {r:0,g:x,b:c};
    	case 4:
    	  return {r:x,g:0,b:c};
    	case 5:
    	  return {r:c,g:0,b:x};
    }
  	
  }

  Object.defineProperty(this,'Hex',{
    enumerable:true,
    configurable:false,
    get:function(){
      return color.Hex;
    },
    set:function(value){
      color.Hex=value;
      HextoRGB();
      RGBtoHSV();
    }
  });
  ['Red','Green','Blue'].forEach(function(element){
    Object.defineProperty(this,element,{
      enumerable:true,
      configurable:false,
      get:function(){
        return color[element];
      },
      set:function(value){
        color[element]=value;
        RGBtoHSV();
        RGBtoHex();
      }
    });
  },this);
  ['Hue','Saturation','Value'].forEach(function(element){
    Object.defineProperty(this,element,{
      enumerable:true,
      configurable:false,
      get:function(){
        return color[element];
      },
      set:function(value){
        color[element]=value;
        HSVtoRGB();
        RGBtoHex();
      }
    });
  },this);


  function RGBtoHSV(){
        var Red=color.Red/255;
    var Green=color.Green/255;
    var Blue=color.Blue/255;
    var ChromaMin=Math.min(Red,Green,Blue);
    var ChromaMax=Math.max(Red,Green,Blue);
    var Delta=ChromaMax-ChromaMin;
    
    if(ChromaMax===Red)
      color.Hue=((Green-Blue)/Delta)%6;
    else if(ChromaMax===Green)
      color.Hue=(Blue-Red)/Delta+2;
    else
      color.Hue=(Red-Green)/Delta+4;
    
    color.Hue*=60;
    color.Value=ChromaMax;
    if(ChromaMax===0){
      color.Saturation=0.0;
      color.Hue=0.0;
    }else
      color.Saturation=Delta/ChromaMax;
  }
  function HSVtoRGB(){
    if(color.Saturation===0){
    	color.Red=color.Green=color.Blue=Math.floor(color.Value*255);
    	return;
    }
    
    var h=color.Hue/60;
    var c=color.Value*color.Saturation;
    var x=c*(1-Math.abs(h%2-1));
    var m=color.Value-c;
    
    var rgb=SwitchCases(h,c,x)
    
    color.Red=Math.floor((rgb.r+m)*255);
    color.Green=Math.floor((rgb.g+m)*255);
    color.Blue=Math.floor((rgb.b+m)*255);
  }
  /*
  this.HSLtoRGB=function(){
    var h=this.Hue/360;
    var q=this.Lightness<0.5?this.Lightness*(1+this.Saturation):this.Lightness+this.Saturation-this.Lightness*this.Saturation;
    var p=2*this.Lightness-q;

    this.Red=Math.floor(hue2rgb(p,q,h+1/3)*255);            
    this.Green=Math.floor(hue2rgb(p,q,h)*255);
    this.Blue=Math.floor(hue2rgb(p,q,h-1/3)*255);
  };
              
  this.RGBtoHSL=function(){
    var Red=color.Red/255;
    var Green=color.Green/255;
    var Blue=color.Blue/255;
    var ChromaMin=Math.min(Red,Green,Blue);
    var ChromaMax=Math.max(Red,Green,Blue);
    var Delta=ChromaMax-ChromaMin;
    
    if(ChromaMax==Red)
      this.Hue=((Green-Blue)/Delta)%6;
    else if(ChromaMax==Green)
      this.Hue=(Blue-Red)/Delta+2;
    else
      this.Hue=(Red-Green)/Delta+4;
    
    this.Hue*=60;
    this.Lightness=(ChromaMax+ChromaMin)/2;
    this.Saturation=Delta?Delta/(1-Math.abs(2*this.Lightness-1)):0;    
  }*/
  
  function RGBtoHex(){
  	var red=color.Red.toString(16);
  	while(red.length<2) red='0'+red;
  	var green=color.Green.toString(16)
  	while(green.length<2) green='0'+green;
  	var blue=color.Blue.toString(16);
  	while(blue.length<2) blue='0'+blue;
    color.Hex=red+green+blue;
  }
  function HextoRGB(){
    color.Red=parseInt(color.Hex.substr(0,2),16);
    color.Green=parseInt(color.Hex.substr(2,2),16);
    color.Blue=parseInt(color.Hex.substr(4,2),16);
  }
}