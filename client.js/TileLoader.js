function TileLoader(){
  this.tileSize=256;
  this.cache=new Array;
  this.tileCount=0;
  
  this.remove=function(path){
    var tile=this.cache[path.Stringify()];
    if(!tile.Tainted){
      delete this.cache[path.Stringify()];
      this.tileCount--;
    }else{
      tile.Flush();
      console.log(path.Stringify()+'is tainted');
    }
  };
  this.fetch=function(path){
    var pathString=path.Stringify();
    var tile;
    if(pathString in this.cache){
      tile=this.cache[pathString];
    }else{
      tile=new Tile(path,'public')
      this.cache[pathString]=tile;
      this.tileCount++;
    }

    var text='Tiles Created: '+this.tileCount+"\n";
    text+='Cache Size: '+bytesToSize(this.tileCount*262144);          
    $('#tilecount').text(text);
    
    return tile;
  };
}