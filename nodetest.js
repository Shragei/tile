var path = require('path');
var fs=require('fs');
var microtime = require('microtime');
var crypto=require('crypto');
var redis=require('redis');
var mongo=require('mongodb');
var BSON=require('bson').pure().BSON;
var express=require('express');
var app=express();
var io=require('socket.io').listen(app.listen(2424),{log:false,transports:['websocket']});


var RC=redis.createClient();

var database=new mongo.Db('test',new mongo.Server('127.0.0.1',27017,{safe:false},{auto_reconnect:true},{}),{w:0});

var Tile=function(database,session){
  
  var collection=database.collection('tiles');
  

  
  this.Fetch=function(tileID,callback){
    collection.findOne({_id:canvas+'-'+tileID},function(err,tile){
      if(!err&&tile){
        var cards=new Array;
        for(var name in tile.cards){
          history=tile.cards[name];
          var card=history[history.length-1];
          if(!session.owner){
            delete card.owner;
            delete card.timestamp;
          }
          card.image=fs.readFileSync(path.join(__dirname,'canvas',session.canvas,card.image))
          cards.splice(history.position,card);
        }
        callback(BSON.serialize(cards));
      }
    });
  }
  
  this.Store=function(tileID,data,callback){
    if(!session.permission.write){
      callback(new Error('No write permission for this canvas'));
    }
    var cards=BSON.deserialize(data);
    if(typeof cards ==='object')    
      for(var name in cards){
        var card=cards[name];
        var shasum = crypto.createHash('sha1');
        var tmpPath=path.join('/tmp',crypto.randomBytes(4).readUInt32LE(0)+
                                  ''+crypto.randomBytes(4).readUInt32LE(0));
        var tmpFile=fs.writeFileSync(tmpPath,card.image);
        shasum.update(card.image,'binary');
        
        var hash=shasum.digest('base64');
        hash=hash.substr(0,hash.length-1).replace(/\//g,'.');
        card.image=hash;
        
        card.owner=session.username;
        card.timetamp=new Date();
        
        var key=new Object;
        key['cards.'+name]=card
        collection.update({_id:session.canvas+'-'+tileID},{
          $push:key
        },{upsert:true});
      }
      collection.update({_id:session.canvas+'-'+tileID},{
        $set:{name:session.canvas,
              tile:tileID
        }
      });
      callback(null)
  }
}

function User(database){
  var uername='';
  var password='';
  
  this.Create=function(name,password){
  
  
  }
  
  this.Canvas=function(){
  
  }
 
}

function Canvas(database,session){
  var collectionCanvas=database.collection('canvas');
  var collectionBookmarks=database.collection('bookmarks');
  var bookmarks=[];
  var canvas='';
  var permissions={};
  
  this.Join=function(CanvasName,callback){
    canvas=CanvasName;
    this.Meta(CanvasName,function(err,meta){
      if(err)
        return callback(err);
      
      if(!permissions.read)
        return callback(new Error('Permission denied to join canvas'));
        
      session.RC.subscribe(CanvasName);
      meta.permissions=permissions;
      callback(undefined,meta);
    });
  
    collectionCanvas.findOne({_id:CanvasName},function(err,canvas){
      if(err){
        callback(err);
        return;
      }
      if(!canvas){
        callback(new Error('Canvas doesn\'t exist'));
        return;
      }

             

      if(!session.permission.read)
        callback(new Error('Permission denied to join canvas'));
      session.RC.subscribe(CanvasName);
      callback();
    });
  }
  
  {Owner:'username',
   Name:'Canvas name',
   Views:0,
   TileCount:0,
   GlobalPermissions:{Read:true,Write:true},
   Users:{'username':{Read:true,Write:false}}
  }
  
  this.Meta=function(canvasName,callback){
    collectionCanvas.find({Name:canvasName},function(err,meta){
      if(!meta)
        return callback(new Error('Canvas doesn\'t exist'));
     
      if(canvas==meta.Name){ //tring to join canvas
        if(session.Username in meta.Users)
          permissions=meta.Users[session.Username];
        permissions.write|=canvas.global.write;
        permissions.read|=canvas.global.read;
        permissions.owner=false;
      }
      
      if(session.Username!=meta.Owner)
         meta.Users={};
      
      callback(undefined,meta);
    }
  }

  {canvas:'name',
   username:'name',
   bookmarks:[{name:'bookmarkname',location:'tileid'}]};
  this.Bookmarks=function(callback){
    collection.find({username:username},function(err,bookmarks){
    
    
    });
  }
  this.BookmarksAdd=function(name,location){
    collectin.set
  
  }
}

function session(RC){

  this.auth=function(){}

}
