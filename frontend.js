﻿var path = require('path');
var fs=require('fs');
var microtime = require('microtime');
var crypto=require('crypto');
var redis=require('redis');
var mongo=require('mongodb').MongoClient;
var BSON=require('bson').pure().BSON;
var express=require('express');
var app=express();
var io=require('socket.io').listen(app.listen(2424),{log:false,transports:['websocket']});

//var RC=redis.createClient();

app.use(function(req,res,next){
 // console.log('%s %s',req.method,req.url);
  next();
});

//console.log(Object.keys(BSON.pure().BSON));

app.use('/client.js',express.static(path.join(__dirname,'client.js')));
app.use('/resources',express.static(path.join(__dirname,'resources')));

function main(req,res){
  console.log('loading canvas %s',req.params.id);
  res.sendfile(path.join(__dirname,'Loader.html'));
};
/*
function tilefetch(req,res){
  var canvas=req.params.canvas;
  var tile=req.params.tile;
  //TODO test the autherization header for malform input
  var token=req.headers['authorization'].split(' ')[1];
  RC.get('session∈'+token);
          //Canvas:Tile
  RC.zrange('Canvas∈'+canvas+'∈'+tile,0,-1,function(err,cardNames){

    if(cardNames.length===0){
  //    console.log("tile: "+req.params.canvas+':'+req.params.tile+' not found');
      res.send(204);
      return;
    }
    var multi=RC.multi();
    
    for(var i=0;i<cardNames.length;i++)
      multi.lindex('Canvas∈'+canvas+'∈'+tile+'∈'+cardNames[i],-1);
                 //canvas:tile:card
    multi.exec(function(err,list){
      var paths=new Array;
      var finished=list.length;
      for(var i=0;i<list.length;i++){
        if(list[i]!==0){
          paths.push({name:cardNames[i],data:fs.readFileSync(path.join(__dirname,'canvas',req.params.canvas,list[i]))});
        }
      }
   //   console.log(paths);
      res.send(BSON.serialize(paths));
    });
  });
}

function tilestore(req,res){
  //console.log(req.header('Authorization'));
 // console.log(JSON.stringify(req.params));
  var order=0.0;
  var user='anon';
  var canvas=req.params.canvas;
  var tile=req.params.tile;
  var card=req.params.card;

  var tmpPath=path.join('/tmp',crypto.randomBytes(4).readUInt32LE(0)+
                            ''+crypto.randomBytes(4).readUInt32LE(0));

  var shasum = crypto.createHash('sha1');
  var tmpFile=fs.createWriteStream(tmpPath,{flags:'a'});

  req.on('data',function(data){
    
    console.log('writing file');
    tmpFile.write(data);
    shasum.update(data,'binary');
  });

  req.on('end',function(){
    
    var hash=shasum.digest('base64');
    console.log(hash);
    hash=hash.substr(0,hash.length-1).replace(/\//g,'.');
    tmpFile.end(function(){
      console.log('moving file: '+path.join(__dirname,'canvas',canvas,hash));
      fs.renameSync(tmpPath,path.join(__dirname,'canvas',canvas,hash));
    });
    
    
    RC.multi() 
    .zadd(['Canvas∈'+canvas+'∈'+tile,order.toString(),card],function(err,added){
      if(added)
        RC.incr('Canvas∈'+canvas+'∈Tiles');
    }).rpush('Canvas∈'+canvas+'∈'+tile+'∈'+card,hash,function(err,pos){
      pos--;
      RC.rpush('User∈'+user+'∈history',canvas+'∈'+tile+'∈'+card+'∈'+pos);

    }).exec(function(err,result){
      res.send(201);
      //respond with error status
    });
    
  });
 // write.on('error',function(err){
 //   console.log(err);
//  });

}
*/
app.get('/',main);
/*
app.get('/canvas',main);
app.get('/canvas/',main);
app.get('/canvas/:canvas',main);

app.get('/canvas/:canvas/:tile',tilefetch);
app.put('/canvas/:canvas/:tile/:card',tilestore);

io.sockets.on('connection',function(socket){
  var session=new Object;
  var auth=0;
  socket.on('UserLogin',function(data){
    auth='qwerty';
	socket.emit(auth);
    RC.get('User∈'+data.username+'∈password',function(err,password){
	  if(password==data.password){
	    
	  }
	});
  });
  socket.on('UserCreate',function(data){
    var username=data.Name;
	var password=data.password;
    RC.exists(username,function(err,exists){
	  if(exists)
	    socket.emit('UserCreate',{status:'ERR',message:'username already exists'});
	  else{
	    RC.set('User∈'+username+'∈password',password,function(err,status){
		  if(status=='OK')
		    socket.emit('UserCreate',{status:'OK'});
		  else
		    socket.emit('UserCreate',{status:'ERR',message:'Falid creating user'});
		  
		});
	  }
	});
  
  });
  
  socket.on('CanvasMeta',function(canvas){
    RC.multi()
	  .get('canvas∈'+canvas+'∈tiles')
	  .get('canvas∈'+canvas+'∈owner')
	  .hgetall('canvas∈'+canvas+'∈permissions')
	  .exec(function(err,meta){
	    console.log(meta);
		socket.emit('CanvasMeta',JSON.parse(meta));
	  });
  });
  
  socket.on('CanvasJoin',function(data){
    redisClient.get('Canvas:'+data+':Meta',function(err,res){
      var meta=JSON.parse(res);
    });
  });

  socket.on('BookmarksGet',function(data){
    RC.lrange('Bookmarks∈'+session.username+'∈'+session.canvas,0,-1,function(err,list){
	  socket.emit('BookmarksGet',list);
	});
  });
  
  socket.on('BookmarksSet',function(data){
 //   if('canvas' in session)
	//  RC.lset('Bookmarks∈'+session.username+'∈'+session.canvas,
  });
 // socket.on('GetTile',function(tile
});*/
console.log('Listening on port 2424');
